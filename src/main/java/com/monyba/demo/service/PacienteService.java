package com.monyba.demo.service;

import com.monyba.demo.model.Paciente;
import com.monyba.demo.repository.IPacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PacienteService implements IPacienteService{

  // Instanciar esto es incorrecto porque crearía instancias según cantidad de peticiones
  // private PacienteRepository repo = new PacienteRepository();

  // Es mejor inyectar el objecto Creado por Spring en nuestra Variable.
  @Autowired
  private IPacienteRepository repo;

  public String saludar(Paciente paciente){
    if (paciente == null) return "NO EXISTE PACIENTE";
    return repo.saludar(paciente);
  }

}
