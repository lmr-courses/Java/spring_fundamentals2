package com.monyba.demo.controller;

import com.monyba.demo.model.Paciente;
import com.monyba.demo.service.IPacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PacienteController {

  // Instanciar esto es incorrecto porque crearía instancias según cantidad de peticiones
  // private PacienteService service = new PacienteService();

  // Es mejor inyectar el objecto Creado por Spring en nuestra Variable.
  @Autowired
  private IPacienteService service;

  @GetMapping
  public String saludar(){
    Paciente paciente = new Paciente(1, "Brian");
    return service.saludar(paciente);
  }

}
