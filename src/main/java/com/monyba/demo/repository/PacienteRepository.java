package com.monyba.demo.repository;

import com.monyba.demo.model.Paciente;
import org.springframework.stereotype.Repository;

@Repository
public class PacienteRepository implements IPacienteRepository{

  public String saludar(Paciente paciente){
    return "Hola " + paciente.getNombre();
  }

}
